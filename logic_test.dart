void main() {
  // Change input below
  String input = '882211';
  validator(input);
}

void validator(String input) {
  bool isPass = true;

  if (input.length == 6) {
    int count = 0;

    for (int i = 0; i < input.length; i++) {
      if (i <= 3) {
        // Check form 2 next character is the same current
        if (input[i] == input[i + 1] && input[i] == input[i + 2]) {
          print('2.input จะต้องกันไม่ให้มีเลขซ้ำติดกันเกิน 2 ตัว');
          isPass = false;
          break;
        }

        // Check from 2 next character + 1 and character + 2
        int currentNumber = int.parse(input[i]);
        if (currentNumber + 1 == int.parse(input[i + 1]) &&
            currentNumber + 2 == int.parse(input[i + 2])) {
          print('3.input จะต้องกันไม่ให้มีเลขเรียงกันเกิน 2 ตัว');
          isPass = false;
          break;
        }
      }

      // Check from have 3 duplicate sets
      if (count >= 3) {
        print('4.input จะต้องกันไม่ให้มีเลขชุดซ้ำ เกิน 2 ชุด');
        isPass = false;
        break;
      }
      if (i <= 4) {
        count += input[i] == input[i + 1] ? 1 : 0;
      }
    }
  } else {
    // Check from character length
    print('1.input จะต้องมีความยาวมากกว่าหรือเท่ากับ 6 ตัวอักษร');
  }

  if (isPass) {
    print('ผ่านทั้ง 4 เงื่อนไข');
  }
}
